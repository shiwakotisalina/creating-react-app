import React, { Component } from 'react';
import './App.css';

import { Register } from './components/register';
import {Header} from './components/Header';


function App() {
  return (
    <div className="App">
   <Header />
      <Register />
   
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        
        <a
        
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
    </div>
  );
}

export default App;
